import time
import datetime
from jnius import autoclass
from decimal import Decimal

Date = autoclass('java.util.Date')
MyJavaClass = autoclass('MyJavaClass')


def to_java_date(python_datetime):
    return Date(int(time.mktime(python_datetime.timetuple()) * 1000))


def from_java_date(java_date):
    return datetime.datetime.fromtimestamp(java_date.getTime() / 1000.0)

my_java_class = MyJavaClass()
my_java_class.testMethod()
my_java_class.testDecimal(Decimal('1.23'))
my_java_class.testDatetime(to_java_date(datetime.datetime.today()))
print('Java returned: {}'.format(my_java_class.testReturn()))

java_return_date_result = my_java_class.testReturnDate()
print('Java returned: {}'.format(java_return_date_result))

print(from_java_date(java_return_date_result))

my_java_class.testPassArray([1, 2, 3])

print(my_java_class.testReturnArray())

java_return_array_list_result = my_java_class.testRetunArrayList()
print(java_return_array_list_result)
print(java_return_array_list_result.toArray())

print('Finished!')
