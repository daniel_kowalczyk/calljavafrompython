# Installation

Must have Java installed.

```
mkvirtualenv --python=/usr/bin/python3 -a /home/daniel/projects/calljavafrompython calljavafrompython
workon calljavafrompython
pip install -r requirements.txt
javac MyJavaClass.java
python main.py
```
