import java.util.Date;
import java.util.ArrayList;
import java.util.List;


public class MyJavaClass {
    public void testMethod() {
        System.out.println("Java says hi!");
    }

    public void testDecimal(double decimal) {
        System.out.println(decimal);
    }

    public void testDatetime(Date datetime) {
        System.out.println(datetime);
    }

    public double testReturn() {
        double ret = 123.456;
        return ret;
    }

    public Date testReturnDate() {
        return new Date();
    }

    public void testPassArray(int[] arr) {
        System.out.println(arr);
        System.out.println(arr[0]);
    }

    public int[] testReturnArray() {
        int[] ret = {1, 2, 3};
        return ret;
    }

    public List<Double> testRetunArrayList() {
        List<Double> ret = new ArrayList<Double>();
        ret.add(0.1);
        ret.add(0.2);
        return ret;
    }

    public static void main(String[] args) {
        System.out.println("Hello, World");
    }
}
